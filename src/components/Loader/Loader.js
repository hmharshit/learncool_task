import React from 'react';
import ContentLoader from "react-content-loader";
export default const Loader = props => (
    <ContentLoader
        height={100}
        width={300}
        speed={1}
        primaryColor="#a83fe4"
        secondaryColor="#92d2ss"
        style={{maxWidth:'400px'}}
        {...props}
    >
    </ContentLoader>
);