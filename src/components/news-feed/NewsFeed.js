import React, {Component} from 'react'
import {Card, Image, Button, Icon, Modal, Form} from 'semantic-ui-react'
import {ApolloConsumer, Query, Mutation} from 'react-apollo'
import gql from 'graphql-tag'
import './NewsFeed.css'
import {Instagram} from 'react-content-loader'


const feed_query = gql`
    {
        allPosts(orderBy: createdAt_DESC) {
            id
            title
            upvotes
            url
            description
        }
    }`;

let POST_MUTATION = gql`
    mutation PostMutation ($title: String!, $description: String!, $url: String!) {
        createPost (title: $title , description: $description, upvotes: 0, url: $url) {
            id
        }
    }`;


let UPVOTE_INCREASE = gql`
    mutation PostMutation ($id: ID!, $upvotes: Int!) {
        updatePost (id: $id , upvotes: $upvotes ) {
            id,
            upvotes
        }
    }`;

class NewsFeed extends Component {
    state = {description: "", url: "", title: ""};

    render() {

        const {title, description, url} = this.state;
        return (<div>

                <div className="modal-button">
                    <Modal trigger={<Button onClick={() => this.setState({isModalVisible: true})}>Create New</Button>}
                           closeIcon>
                        <Modal.Header>Post News</Modal.Header>
                        <Modal.Content>
                            <div>
                                <div>
                                    <Form>
                                        <Form.Field>
                                            <label>Title</label>
                                            <input
                                                value={title}
                                                onChange={e => this.setState({title: e.target.value})}
                                                type="text"
                                                placeholder="A title for the link"
                                            />
                                        </Form.Field>
                                        <Form.Field>
                                            <label>Descrption</label>
                                            <input
                                                value={description}
                                                onChange={e => this.setState({description: e.target.value})}
                                                type="text"
                                                placeholder="A description for the link"
                                            />
                                        </Form.Field>
                                        <Form.Field>
                                            <label>Url</label>
                                            <input
                                                value={url}
                                                onChange={e => this.setState({url: e.target.value})}
                                                type="text"
                                                placeholder="The URL for the link"
                                            />
                                        </Form.Field>
                                        <Form.Field/>
                                    </Form>
                                </div>

                                {/*<Mutation*/}
                                {/*mutation={POST_MUTATION}*/}
                                {/*variables={{title}}*/}
                                {/*>*/}
                                {/*{postMutation => <button onClick={postMutation}>Submit</button>}*/}
                                {/*</Mutation>*/}

                                <ApolloConsumer>
                                    {client => (
                                        <Button type='submit' onClick={
                                            async () => {
                                                await client.mutate({
                                                    mutation: POST_MUTATION,
                                                    variables: {
                                                        title: this.state.title,
                                                        description: this.state.description,
                                                        url: this.state.url
                                                    }
                                                }).then(res => {
                                                    if (res.data.createPost != null) {
                                                        console.log("in if")
                                                    } else {
                                                        console.log("in else");
                                                    }
                                                }).catch(error => {
                                                    console.log(error)
                                                });
                                                this.setState({isModalVisible: false});
                                                window.location.reload();
                                            }

                                        }>Submit
                                            News</Button>

                                    )}
                                </ApolloConsumer>

                            </div>
                        </Modal.Content>
                    </Modal>
                </div>
                <Query query={feed_query}>
                    {({loading, error, data}) => {
                        if (loading) return <Instagram/>;
                        if (error) return <div>Error</div>;
                        return (
                            <div className="newsfeed-main">
                                <Card.Group className="newsfeed-card" style={{width : '70%'}}>
                                    {
                                        data.allPosts.map((x) => {
                                            return (
                                                <Card fluid color='red'>
                                                    <Card.Content>
                                                        <div>
                                                            <div className="newsfeed-content">
                                                                <Card.Header><span
                                                                    className='date'><h2>{x.title}</h2></span>
                                                                </Card.Header>
                                                                <Card.Description>{x.description}</Card.Description>

                                                            </div>
                                                            <div className="newsfeed-image newsfeed-url">
                                                                <Image src={x.url} style={{'max-height' : '150px'}} />
                                                            </div>
                                                        </div>
                                                    </Card.Content>
                                                    <Card.Header>
                                                        <Mutation mutation={UPVOTE_INCREASE} variables={{
                                                            id: x.id,
                                                            upvotes: parseInt(x.upvotes) + 1
                                                        }}>
                                                            {postMutation =>
                                                                <Button
                                                                    onClick={postMutation}

                                                                    color="white"
                                                                    content={<Icon name="thumbs up outline"
                                                                                   size="large"/>} circular
                                                                    className="newsfeed-btn"/>}
                                                        </Mutation>

                                                        {x.upvotes}


                                                    </Card.Header>
                                                </Card>
                                            )
                                        })
                                    }
                                </Card.Group>
                            </div>
                        )
                    }}
                </Query>
            </div>
        )
    }
}

export default NewsFeed;