import React, {Component} from 'react'
import NewsFeed from '../news-feed/NewsFeed'
import './Main.css'

class Main extends Component {
    render() {
        return (
            <div className="main-layout">
                <NewsFeed/>
            </div>
        )
    }
}

export default Main
